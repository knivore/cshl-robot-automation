#!/usr/bin/env bash
# ===============================================================================
# Robot Automation - CARESHIELD LIFE PRODUCTION SANITY TEST
#
# Prerequisite: Ensure ROBOT_TEST_HOME is defined.
#              (export ROBOT_TEST_HOME={/PATH_TO_YOUR_ROBOT_PROJ_DIRECTORY})
#
# ===============================================================================

## ======== UPDATE THE PATH TO YOUR PROJECT LOCATION ========
export ROBOT_TEST_HOME=C:/Users/chink/Documents/Git/cshl-robot-automation
## ==========================================================

export PYTHONUNBUFFERED=1

robot -T \
  --log log.html \
  --outputdir $ROBOT_TEST_HOME \
  --variable BROWSER:Chrome \
  -A $ROBOT_TEST_HOME/config/cshl-prod.properties $ROBOT_TEST_HOME/testcase/SanityTest.robot > $ROBOT_TEST_HOME/ProdSanityTestConsoleLog-$(date +%Y%m%d%H%M%S).txt

foldername=CSHL-PROD-$(date +%Y%m%d%H%M%S)
mkdir -p  $ROBOT_TEST_HOME/testrun/"$foldername"
readonly destination=$ROBOT_TEST_HOME/testrun/"$foldername"

while read line; do
  echo 'name: ['${line}'], basename: ['${line##*/}']'
  mv ${line} ${destination}
done < <(find $ROBOT_TEST_HOME -maxdepth 1 -name '*Log*.txt')

while read line; do
  echo 'name: ['${line}'], basename: ['${line##*/}']'
  mv ${line} ${destination}
done < <(find $ROBOT_TEST_HOME -maxdepth 1 -name '*.html')

while read line; do
  echo 'name: ['${line}'], basename: ['${line##*/}']'
  mv ${line} ${destination}
done < <(find $ROBOT_TEST_HOME -maxdepth 1 -name '*.png')

while read line; do
  echo 'name: ['${line}'], basename: ['${line##*/}']'
  mv ${line} ${destination}
done < <(find $ROBOT_TEST_HOME -maxdepth 1 -name '*.xml')

sleep 20