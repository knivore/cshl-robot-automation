*** Settings ***
Documentation   An overall comprehensive test suite testing for the following functionality listed:
...             Login, Homepage Verification & everything related to basic webpage sanity test.
Metadata        Version    0.1

Resource        %{ROBOT_TEST_HOME}/resource/commonUtil.robot
Resource        %{ROBOT_TEST_HOME}/resource/homepage.robot
Resource        %{ROBOT_TEST_HOME}/resource/login.robot

Library         SeleniumLibrary        run_on_failure=Nothing

Suite Setup     commonUtil.Open Browser To Home Page
Suite Teardown  Close Browser
Test Teardown   commonUtil.Test Teardown

## NOTE: There is no need to declare the file for each of the keywords. E.g: commonUtil.Debug Log
## However, the declaration is for cleaner and easier reference to where the keyword is from, SeleniumLibrary, Robot or Self built.


*** Test Cases ***

Sanity Test Start
    commonUtil.Debug Log  \n========================================= Sanity Test Started =========================================


Website Accessibility Verification
## To write new keyword to test whether connection to website is ok
    commonUtil.Verify Page Found

# ========================================= Home Page Layout Verification =========================================

Homepage Layout Verification
    homepage.Title Verification
    homepage.Header Verification
    homepage.Footer Verification
    homepage.CSHL Logo Verification
    homepage.MOH Logo Verification


# ========================================= Login Verification =========================================

Login Verification
    login.Go To Login Page





Sanity Test End
    commonUtil.Debug Log  \n========================================= Sanity Test Completed =========================================

