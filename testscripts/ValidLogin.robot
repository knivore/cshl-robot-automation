*** Settings ***
Documentation   A resource file with reusable keywords and variables for CSHL Login.
...             The system specific keywords created here form our own
...             domain specific language. They utilize keywords provided
...             by the imported SeleniumLibrary.

Resource        %{ROBOT_TEST_HOME}/utils/commonUtil.robot
Resource        %{ROBOT_TEST_HOME}/testscripts/login.robot

Library         SeleniumLibrary        run_on_failure=Nothing

Suite Setup     commonUtil.Open Browser To Home Page
Suite Teardown  Close Browser
Test Setup      Go To Login Page
Test Template   Login With Valid Credentials Should Pass
Test Teardown   commonUtil.Test Teardown

## NOTE: There is no need to declare the file for each of the keywords. E.g: commonUtil.Debug Log
## However, the declaration is for cleaner and easier reference to where the keyword is from, SeleniumLibrary, Robot or Self built.


*** Test Cases ***      USERNAME                PASSWORD
User 1                  S8942892J               ${VALID PASSWORD}


*** Keywords ***
Login With Valid Credentials Should Pass
    [Arguments]  ${username}  ${password}
    Input Username  ${username}
    Input Password  ${password}
    Submit SingPass Login Credentials
    Login Should Have Pass

Login Should Have Pass
    Location Should Be  ${eServiceURL}
    Title Should Be  e-Service

