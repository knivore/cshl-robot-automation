*** Settings ***

Library        SeleniumLibrary        run_on_failure=Nothing
Library        OperatingSystem
Library        String

*** Keywords ***

Write File
    [Arguments]  ${filePath}  ${filename}  ${variable}
    ${stringValue}=  Convert To String  ${variable}
    Create File  ${filePath}/${filename}.txt  ${stringValue}

#========================================================================================================

Append File
    [Arguments]  ${filePath}  ${filename}  ${variable}
    ${stringValue}=  Convert To String  ${variable}
    Append To File  ${filePath}/${filename}.txt  ${stringValue}

#========================================================================================================

Read File
    [Arguments]  ${filePath}  ${filename}
    ${fileExistsStatus}=  File Exists  ${filePath}  ${filename}
    Run Keyword If  "${fileExistsStatus}"!="True"  Write File  ${filePath}  ${filename}  1
    ${contents}=  Get File  ${filePath}/${filename}.txt
    [Return]  ${contents}

#========================================================================================================

File Exists
    [Arguments]  ${filePath}  ${filename}
    ${status}=  Run Keyword And Return Status  File Should Exist  ${filePath}/${filename}.txt
    [Return]  ${status}

#========================================================================================================
