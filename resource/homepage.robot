*** Settings ***
Documentation   A resource file with reusable keywords and variables for CSHL Homepage.
...             The system specific keywords created here form our own
...             domain specific language. They utilize keywords provided
...             by the imported SeleniumLibrary.

Resource        %{ROBOT_TEST_HOME}/resource/commonUtil.robot

Library         SeleniumLibrary        run_on_failure=Nothing

## NOTE: There is no need to declare the file for each of the keywords. E.g: commonUtil.Debug Log
## However, the declaration is for cleaner and easier reference to where the keyword is from, SeleniumLibrary, Robot or Self built.


*** Keywords ***

Title Verification
    Title Should Be  ${homepageTitle}

Header Verification
    Wait Until Page Contains Element  //div[contains(@class,"cshl-logo-holder")]//img  timeout=5s
    Wait Until Page Contains Element  //div[contains(@class,"moh-logo-holder")]//img  timeout=5s

    Page Should Contain Element  //div[contains(@class,"cshl-logo-holder")]//img
    Page Should Contain Element  //div[contains(@class,"moh-logo-holder")]//img

    Page Should Contain Element  xpath=//*[@class="navbar-menu"]

CSHL Logo Verification
    ${cshlLogo}=  Set Variable  //div[contains(@class,"cshl-logo-holder")]//img
    Wait Until Page Contains Element    ${cshlLogo}  timeout=8s
    Element should be visible           ${cshlLogo}
    ${img src}=  Selenium2Library.Get Element Attribute   ${cshlLogo}@src
    Create session   img-src   ${img src}
    ${response}=  Get  img-src  /   ## URL is relative to the session URL
    Should be equal as integers  ${response.status_code}  200  image url '${img src}' returned unexpected status code '${response.status_code}'   show_values=False
    Mouse Over                          ${cshlLogo}
    Click Element                       ${cshlLogo}
    commonUtil.Debug Log  \nVerified CSHL Logo is clickable and visible.

MOH Logo Verification
    ${mohLogo}=  Set Variable  //div[contains(@class,"moh-logo-holder")]//img
    Wait Until Page Contains Element    ${mohLogo}  timeout=8s
    Element should be visible           ${mohLogo}
    ${img src}=  Selenium2Library.Get Element Attribute   ${mohLogo}@src
    Create session   img-src   ${img src}
    ${response}=  Get  img-src  /   ## URL is relative to the session URL
    Should be equal as integers  ${response.status_code}  200  image url '${img src}' returned unexpected status code '${response.status_code}'   show_values=False
    Mouse Over                          ${mohLogo}
    Click Element                       ${mohLogo}
    commonUtil.Debug Log  \nVerified CSHL Logo is clickable and visible.

Links Verification
#    Wait Until Page Contains Element  //*[@id="main-navigation-links"]  timeout=8s
#    ${numberOfLinks}=  Get Matching Xpath Count  //*[@id="main-navigation-links"]/li
#
#    :FOR    ${INDEX}    IN RANGE    1    ${numberOfLinks}+1
#        \    Log  ${INDEX}
#        \    ${lineText}=  Get Text    //*[@id="main-navigation-links"]/li[${INDEX}]/a[1]
#        \    Log  ${lineText}
#        \    ${linklength}    Get Length    ${lineText}
#        \    Run Keyword If    ${linklength}>1    Click Link  //*[@id="main-navigation-links"]/li[${INDEX}]/a[1]
#        \    commonUtil.Is Page a 404 Error
#        \    Go To Home Page via URL
#        \    Logging  \nVerified Main navigation links - ${lineText}



Footer Verification
    Wait Until Page Contains Element  //div[@id="footer"]  timeout=5s
#    Element should be visible           ${}
    commonUtil.Debug Log  Footer Verified

