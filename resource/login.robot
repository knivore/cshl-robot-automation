*** Settings ***
Documentation   A resource file with reusable keywords and variables for CSHL Login.
...             The system specific keywords created here form our own
...             domain specific language. They utilize keywords provided
...             by the imported SeleniumLibrary.

Resource        %{ROBOT_TEST_HOME}/resource/commonUtil.robot

Library         SeleniumLibrary        run_on_failure=Nothing

## NOTE: There is no need to declare the file for each of the keywords. E.g: commonUtil.Debug Log
## However, the declaration is for cleaner and easier reference to where the keyword is from, SeleniumLibrary, Robot or Self built.


*** Keywords ***

Go To Login Page
    Wait Until Page Contains Element  //div[contains(@class,"login_container")]//button  timeout=5s
    Click Button  Login
    commonUtil.Verify Page Found
    commonUtil.Debug Log  SingPass Login Page Verified

Submit SingPass Login Credentials
    Click Button    login_button

