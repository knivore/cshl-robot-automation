*** Settings ***
Documentation   A resource file with reusable keywords and variables for CSHL Login.
...             The system specific keywords created here form our own
...             domain specific language. They utilize keywords provided
...             by the imported SeleniumLibrary.

Resource        %{ROBOT_TEST_HOME}/resource/commonUtil.robot

Library         SeleniumLibrary        run_on_failure=Nothing

## NOTE: There is no need to declare the file for each of the keywords. E.g: commonUtil.Debug Log
## However, the declaration is for cleaner and easier reference to where the keyword is from, SeleniumLibrary, Robot or Self built.


*** Keywords ***

Go To Premium Calculator Page
    Go to  https://hc-stg.careshieldlife.gov.sg/check-my-premium/premium-calculator.html
    commonUtil.Verify Page Found
    commonUtil.Debug Log  Premium Calculator Page Verified

Submit SingPass Login Credentials
    Click Button    login_button

