*** Settings ***

Library         SeleniumLibrary        run_on_failure=Nothing
Library         DateTime
Library         OperatingSystem
Library         Collections
Library         String

*** Keywords ***

Open Browser To Home Page
    Open Browser  ${homepageURL}  ${browser}
    Maximize Browser Window
    Set Test Suite Speed
    Set Test Suite Timeout

Set Test Suite Timeout
    Log To Console  In Method Call Set Test Suite Timeout
    Set Selenium Timeout  ${timeOut}

Set Test Suite Speed
    Log To Console  In Method Call Set Test Suite Speed
    Set Selenium Speed  ${seleniumSpeed}

Test Teardown
    Run Keyword If Test Failed  SeleniumLibrary.Capture Page Screenshot

Pause Robot
    Pause Execution

#========================================================================================================

Get Date Now
    ${timeNow}=  Get Current Date  time_zone=UTC  increment=8 hours  result_format=%Y.%m.%d
# BuiltIn.Log To Console  \n${timeNow}
    [Return]  ${timeNow}

#========================================================================================================

Get Date Now File Format
    [Arguments]  ${result_format}=%d%m%Y
    ${timeNow}=  Get Current Date  time_zone=UTC  increment=9 hours  result_format=${result_format}
    [Return]  ${timeNow}

#========================================================================================================

Minus X days
    [Arguments]  ${numberOfDays}=1  ${result_format}=%d%m%Y
    ${numberOfSGHours}=  Evaluate  24 * ${numberOfDays}
    ${minusXDays}=  Get Current Date  time_zone=UTC  increment=-${numberOfSGHours} hours  result_format=${result_format}
    [Return]  ${minusXDays}

#========================================================================================================

Plus X days
    [Arguments]  ${numberOfDays}=1  ${result_format}=%d%m%Y
    ${numberOfSGHours}=  Evaluate  24 * ${numberOfDays}
    ${plusXDays}=  Get Current Date  time_zone=UTC  increment=+${numberOfSGHours} hours  result_format=${result_format}
    [Return]  ${plusXDays}

#========================================================================================================

Verify Page Found
    ${status}=  Run Keyword And Return Status  Page Should Contain Element  //div[contains(@class,"errormessage")]//div[@class="system-message-holder"]
    ${currentURLLocation}=  Get Location
    ${dateTimeNow}=  Get Date Now File Format  %d%m%Y%H%M%S
    Run Keyword If  ${status}  Run Keywords  Error Log  \nError 404 for ${currentURLLocation}.  AND  SeleniumLibrary.Capture Page Screenshot  Error-404-${dateTimeNow}.png
    ...         ELSE  Debug Log  \nVerified that ${currentURLLocation} is not 404.
    [Return]  ${status}

#========================================================================================================

Verify Page Not Found
    ${status}=  Run Keyword And Return Status  Page Should Contain Element  //div[contains(@class,"errormessage")]//div[@class="system-message-holder"]
    ${currentURLLocation}=  Get Location
    Run Keyword If  ${status}  Debug Log  \nVerified that ${currentURLLocation} is supposed to be 404 Page Error.  ELSE  Error Log  \nFailed to verify that ${currentURLLocation} is supposed to be 404 Page Error.
    [Return]  ${status}

#========================================================================================================

# Loop thru all windows and close them if the window is not at the home page by checking the url
Close Other Windows Except For Certain Window Url and https
    [Arguments]  ${windowUrl}
    @{handles}=  List Windows
    :FOR    ${h}    IN    @{handles}
        \    Select Window  ${h}
        \    ${currentURLLocation}=  Get Location
        \    ${https}=   Get Lines Containing String  ${currentURLLocation}  https:
        \    ${httpsLength}=   Get Length  ${https}
        \    Run Keyword If  "${currentURLLocation}"!="${windowUrl}" and ${httpsLength}<1  Close Window

# Loop thru all windows and close them if the window is not at the home page by checking the title
Close Other Windows Except For Certain Window Url
    [Arguments]  ${windowUrl}
    @{handles}=  List Windows
    :FOR    ${h}    IN    @{handles}
        \    Select Window  ${h}
        \    ${currentURLLocation}=  Get Location
        \    Run Keyword If  "${currentURLLocation}"!="${windowUrl}"  Close Window

#========================================================================================================

Debug Log
    [Arguments]  ${text}
    [Documentation]  This method logs to both console output, (IntelliJ Path)/Contents/bin/log.html file & shell script will output entire console log into a txt file

    Log To Console  ${text}     ## Output to terminal
    Log  ${text}                ## Output to log.html

    ## Remove the need to output to a daily log as console log will be export to a text file.

    #${dateNow}=  Get Date Now File Format    %d%m%Y
    #${dateTimeNow}=  Get Date Now File Format    %d/%m/%Y %H:%M:%S
    #${filename}=  Catenate  ${DailyLogFilename}__${dateNow}
    #${data}=  Catenate  \n${dateTimeNow} : ${text}
    #${fileExistsStatus}=  File Exists  %{ROBOT_TEST_HOME}  ${filename}
    #Run Keyword If  ${fileExistsStatus}  Append File  %{ROBOT_TEST_HOME}  ${filename}  ${data}  ELSE  Write File  %{ROBOT_TEST_HOME}  ${filename}  ${data}

Error Log
    [Arguments]  ${text}
    [Documentation]  This method logs to both console output, (IntelliJ Path)/Contents/bin/log.html file & a error log file

    ${dateNow}=  Get Date Now File Format  %d%m%Y
    ${dateTimeNow}=  Get Date Now File Format  %d/%m/%Y %H:%M:%S
    ${filename}=  Catenate  ${ErrorLogFilename}  ${dateNow}
    ${data}=  Catenate  \n${dateTimeNow} : ${text}
    ${fileExistsStatus}=  File Exists  %{ROBOT_TEST_HOME}  ${filename}
    Run Keyword If  ${fileExistsStatus}  Append File  %{ROBOT_TEST_HOME}  ${filename}  ${data}  ELSE  Write File  %{ROBOT_TEST_HOME}  ${filename}  ${data}
    #Set Test Message  \n${text}.  append=yes
    Debug Log  \n${text}.

#========================================================================================================

Parameter Link Spilt URL
    [Arguments]  ${url}  ${spilt}=?  ${firstSpiltPos}=-1  ${secondSpiltPos}=-1
    @{splitDeepLinkURL}=  Split String  ${url}  ${spilt}
    ${urlWithoutDeepLink}=  Get From List  ${splitDeepLinkURL}  ${firstSpiltPos}  ## Get first position

    @{splitURL}=  Split String  ${urlWithoutDeepLink}  /
    ${storeURLName}=  Get From List  ${splitURL}  ${secondSpiltPos}
    [Return]  ${storeURLName}

Normal Link Spilt URL
    [Arguments]  ${url}  ${pos}=-1
    @{splitURL}=  Split String  ${url}  /
    ${storeURLName}=  Get From List  ${splitURL}  ${pos}
    [Return]  ${storeURLName}

#========================================================================================================

Scroll Page To Location
    [Arguments]  ${x_location}  ${y_location}
    Execute JavaScript  window.scrollTo(${x_location},${y_location})

Scroll Page To Element
    [Arguments]  ${element}
    ${y_location}=  Get Vertical Position  ${element}
    ${x_location}=  Get Horizontal Position  ${element}
    Scroll Page To Location  ${x_location}  ${y_location}

