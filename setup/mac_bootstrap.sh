#!/usr/bin/env bash

## set not interactive
export DEBIAN_FRONTEND=noninteractive

apt-get update

## install software
apt-get install -yf python-pip build-essential g++ flex bison gperf ruby perl libsqlite3-dev libfontconfig1-dev libicu-dev libfreetype6 libssl-dev libpng-dev libjpeg-dev python git build-essential chrpath libssl-dev libxft-dev libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev libpq-dev python-dev

python -m pip install --upgrade pip

## install robot
pip install robotframework

## install selenium library
## check here for keyword docs https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
pip install robotframework-seleniumlibrary

## install http library
pip install --upgrade robotframework-httplibrary

## install json schema support
pip install jsonschema

## Install Python requests Library
pip install requests

## Install HTTP requests robot framework Library
pip install -U robotframework-requests

## Install Browser Drivers
pip install webdrivermanager
webdrivermanager firefox chrome --linkpath /usr/local/bin

## install phantom js on Mac
cd ~
export PHANTOM_JS="phantomjs-2.1.1-linux-x86_64"
wget https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2 -nv
sudo tar xvjf $PHANTOM_JS.tar.bz2
mv $PHANTOM_JS /usr/local/share
ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin


