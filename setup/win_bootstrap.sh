#!/usr/bin/env bash

## ======== UPDATE THE PATH TO YOUR PROJECT LOCATION ========
export ROBOT_TEST_HOME=C:/Users/chink/Documents/Git/cshl-robot-automation
## ==========================================================

python -m pip install --upgrade pip

## install robot
pip install robotframework

## install selenium library
## check here for keyword docs https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
pip install --upgrade robotframework-seleniumlibrary

## install http library
pip install --upgrade robotframework-httplibrary

## install json schema support
pip install jsonschema

## Install Python requests Library
pip install requests

## Install HTTP requests robot framework Library
pip install -U robotframework-requests

## Install Chromedriver
pip install webdrivermanager
webdrivermanager firefox chrome --linkpath $ROBOT_TEST_HOME\bin


