# README #

This README document whatever steps are necessary to get this robot automation tool up and running on your pc.

### What is this repository for? ###

* This is an RPA tool to periodically run a sanity test on CareShield Life website checking for anomalies.  
* Current Version: 0.1


### How do I get set up? ###

* How to setup?

1) Download & install Python from (https://www.python.org/downloads/) - Remember to let the installer update your $PATH
2) Update the ROBOT_TEST_HOME path in setup/win_bootstrap.sh
3) Run setup/bootstrap.sh to install the rest of the software.

* How to run tests?
1) Update the ROBOT_TEST_HOME path in /scripts/SanityTest.robot 
2) Double click on the SanityTest.sh


### What software is this build with? ###

* Python
* Robot Framework
* Selenium


### Who do I talk to? ###

* chinkeh@hotmail.com